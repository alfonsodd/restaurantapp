class AddDinerForeignKeyToOrder < ActiveRecord::Migration[5.1]
  def change

    add_index :orders, :diner_id

  end
end
