class AddDinerForeignKeyToSchedule < ActiveRecord::Migration[5.1]
  def change

    add_index :schedules, :diner_id

  end
end
