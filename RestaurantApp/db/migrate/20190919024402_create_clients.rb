class CreateClients < ActiveRecord::Migration[5.1]
  def change
    create_table :clients do |t|
      
      t.string :nombres
      t.string :apellidos

      t.timestamps
    end
  end
end
