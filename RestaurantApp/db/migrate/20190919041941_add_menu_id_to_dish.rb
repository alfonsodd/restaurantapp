class AddMenuIdToDish < ActiveRecord::Migration[5.1]
  def change
    add_column :dishes, :menu_id, :integer
  end
end
