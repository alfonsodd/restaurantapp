class AddMenuForeignKeyToDish < ActiveRecord::Migration[5.1]
  def change

    add_index :dishes, :menu_id

  end
end
