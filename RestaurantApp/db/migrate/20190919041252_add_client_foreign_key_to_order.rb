class AddClientForeignKeyToOrder < ActiveRecord::Migration[5.1]
  def change

    add_index :orders, :client_id

  end
end
