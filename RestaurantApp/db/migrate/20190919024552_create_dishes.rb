class CreateDishes < ActiveRecord::Migration[5.1]
  def change
    create_table :dishes do |t|
      
      t.decimal :precio
      t.string :descripcion
      
      t.timestamps
    end
  end
end
