class CreateOrders < ActiveRecord::Migration[5.1]
  def change
    create_table :orders do |t|

      t.string :hora
      t.string :delivery
      t.decimal :costo
      t.date :fecha
      t.boolean :estado
      

      t.timestamps
    end
  end
end
