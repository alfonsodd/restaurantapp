class AddDinerIdToOrder < ActiveRecord::Migration[5.1]
  def change
    add_column :orders, :diner_id, :integer
  end
end
