class AddDinerIdToMenu < ActiveRecord::Migration[5.1]
  def change
    add_column :menus, :diner_id, :integer
  end
end
