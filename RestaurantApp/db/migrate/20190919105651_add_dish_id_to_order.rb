class AddDishIdToOrder < ActiveRecord::Migration[5.1]
  def change

    add_column :orders, :dish_id, :integer

  end
end
