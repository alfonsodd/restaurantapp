class AddDishForeignKeyToOrder < ActiveRecord::Migration[5.1]
  def change
    add_index :orders, :dish_id
  end
end
