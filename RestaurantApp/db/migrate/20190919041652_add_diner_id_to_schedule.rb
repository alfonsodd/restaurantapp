class AddDinerIdToSchedule < ActiveRecord::Migration[5.1]
  def change
    add_column :schedules, :diner_id, :integer
  end
end
