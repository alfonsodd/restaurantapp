class AddUserIdToDiner < ActiveRecord::Migration[5.1]
  def change
    add_column :diners, :user_id, :integer
  end
end
