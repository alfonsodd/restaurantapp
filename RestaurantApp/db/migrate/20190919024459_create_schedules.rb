class CreateSchedules < ActiveRecord::Migration[5.1]
  def change
    create_table :schedules do |t|

      t.date :fecha
      t.integer :apertura
      t.integer :cierre

      t.timestamps
    end
  end
end
