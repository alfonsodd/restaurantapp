class AddDinerForeignKeyToMenu < ActiveRecord::Migration[5.1]
  def change

    add_index :menus, :diner_id

  end
end
