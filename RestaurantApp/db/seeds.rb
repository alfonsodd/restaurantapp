# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.create(email: 'alfonso@gmail.com', password:'123456',tipo: 'cliente')
User.create(email: 'restaurante1@gmail.com', password:'123456',tipo: 'restaurante')
User.create(email: 'restaurante2@gmail.com', password:'123456',tipo: 'restaurante')

Client.create(nombres: 'Alfonso', apellidos: 'Diaz', user_id:1)
Diner.create(nombre: 'Restaurante 1', direccion: 'Direccion #125', user_id:2)
Diner.create(nombre: 'Restaurante 2', direccion: 'Direccion #256', user_id:3)

Menu.create(fecha: Time.now.strftime("%Y/%m/%d"), diner_id: 1)
Dish.create(precio: 10.0, descripcion: 'Arroz Chaufa', menu_id: 1)
Dish.create(precio: 10.0, descripcion: 'Causa Chaufa', menu_id: 1)

Schedule.create(fecha: Time.now.strftime("%Y/%m/%d"),apertura: 8, cierre: 16, diner_id: 1)