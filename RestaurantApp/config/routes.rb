Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root :to => 'auth#inicio'
  post 'auth/cerrar_sesion'
  get 'client/diners'
  post 'auth/login'
  post 'auth/register'
  
 
  get 'diner/orders'
  get 'diner/menu'

  post 'diner/entrega'
  post 'client/menu'
  post 'client/order'
  post 'dish/add_dish'
  post 'dish/delete_dish'
  post 'diner/atencion'
  post 'schedule/update_schedule'
  post 'order/update_order'


end
