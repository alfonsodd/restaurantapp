class Menu < ApplicationRecord
    belongs_to :diner
    has_many :dishes


    def self.get_menu_today(dinerid)
        hoy = Time.now.strftime("%Y/%m/%d")
        menu = Menu.find_by(fecha: hoy, diner_id: dinerid) 

        if menu == nil
            menu = Menu.create(fecha: hoy, diner_id: dinerid)
        end

        return menu
    end

end
