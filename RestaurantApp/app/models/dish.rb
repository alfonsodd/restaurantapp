class Dish < ApplicationRecord
    belongs_to :menu
    has_one :order

    def self.get_dishes_today(menuid)
        return Dish.where(menu_id: menuid)
    end

end
