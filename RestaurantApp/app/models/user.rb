class User < ApplicationRecord
    has_secure_password
    validates_uniqueness_of :email

    has_many :diners
    has_many :clients

    

    def self.login(p_email,p_pass)
        user = User.find_by(email: p_email).try(:authenticate,p_pass)
        if(user != nil)
            return user
        else
            return false
        end
    end


    def self.register(p_email,p_pass,p_nombres,p_apellidos)
        user = User.create(email: p_email, password: p_pass, tipo: "cliente" )
        Client.create!(nombres:p_nombres,apellidos: p_apellidos, user_id: user.id)
    end

end
