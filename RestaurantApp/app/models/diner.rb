class Diner < ApplicationRecord
    belongs_to :user
    has_many :orders
    has_many :menus
    has_many :dishes
    has_many :schedules

    def self.get_diners_today
        hoy = Time.now.strftime("%Y/%m/%d")
        Diner.joins(:menus).where(menus:{fecha: hoy})
    end

end
