class Order < ApplicationRecord
    belongs_to :client
    belongs_to :diner
    has_one :dish

    def self.get_orders_today(dinerid)
        hoy = Time.now.strftime("%Y/%m/%d")
        return Order.where(diner_id: dinerid, fecha: hoy, estado: 0 )
    end

    def self.generar_codigo(clientid)
            order = Order.where(client_id:clientid).last
            url = "http://chart.apis.google.com/chart?cht=qr&chs=300x300&chl=" + "idPedido: " + order.id.to_s + " || idCliente: " + order.client_id.to_s  + " || Delivery: " + order.delivery + " || Costo:" + order.costo.to_s
            return url
    end
end
