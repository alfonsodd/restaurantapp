class Schedule < ApplicationRecord
    belongs_to :diner

    def self.get_schedule_today(dinerid)
        hoy = Time.now.strftime("%Y/%m/%d")
        schedule = Schedule.find_by(diner_id: dinerid, fecha: hoy)

        if schedule ==  nil
            schedule = Schedule.create(fecha: hoy, apertura: 12, cierre: 18, diner_id:  dinerid)
        end
        
        return schedule
    end

    def self.get_atencion(dinerid)
        schedule = get_schedule_today(dinerid)
        hora_actual = Time.now.strftime("%H").to_i
    
        if(hora_actual < schedule.apertura )
            hora_actual = schedule.apertura
        end
        
        return [hora_actual,schedule.cierre]

    end

end
