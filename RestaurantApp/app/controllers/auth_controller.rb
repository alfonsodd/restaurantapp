class AuthController < ApplicationController
    skip_before_action :verify_authenticity_token

    def inicio
    end
    def login
        user = User.login(params[:correo], params[:pass])
        if(user and user.tipo == 'cliente')
            session[:usuario_activo] = user.clients.first.id
            session[:tipo_usuario] = user.tipo
            session[:usuario_nombre] = user.clients.first.nombres
            redirect_to client_diners_path
        elsif(user and user.tipo == 'restaurante')
            session[:usuario_activo] = user.diners.first.id
            session[:tipo_usuario] = user.tipo
            session[:usuario_nombre] = user.diners.first.nombre
            redirect_to diner_orders_path
        else
            flash[:message] = "Usuario incorrecto por favor intente nuevamente"
            redirect_to root_path
        end
    end

    def registers
        User.register(params[:email],params[:password],params[:nombres], params[:apellidos])
        redirect_to root_path
    end

    def cerrar_sesion
        session.clear
        redirect_to root_path
    end


end
