class DinerController < ApplicationController
    skip_before_action :verify_authenticity_token

    def orders
        @orders = Order.get_orders_today(session[:usuario_activo])
    end

    def menu
        @menu = Menu.get_menu_today(session[:usuario_activo])
        session[:menu_activo] = @menu.id
        @schedule = Schedule.get_atencion(session[:usuario_activo]) 
    end



end
