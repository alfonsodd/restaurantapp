class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def check_session
    if(session[:usuario_activo] == nil)
      redirect_to root_path
    end
  end
  helper_method :check_session
end
