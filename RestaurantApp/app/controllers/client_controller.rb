class ClientController < ApplicationController
    skip_before_action :verify_authenticity_token
    before_action :authenticate_with_http_basic

    require 'open-uri'
    def diners
        @diners = Diner.get_diners_today
    end

    def menu
        @menu  = menu_hoy = Menu.get_menu_today(params[:res])
        session[:restaurante_activo] = params[:res]
        @atencion = Schedule.get_atencion(menu_hoy.diner_id)
    end

    def order
        hora = params[:hora] unless params[:delivery] == "Si"
        Order.create(fecha: Time.now.strftime("%Y/%m/%d") ,hora: hora.to_s, delivery: params[:delivery],costo:Dish.find(params[:iddish]).precio,estado:0,client_id: session[:usuario_activo],diner_id:session[:restaurante_activo],dish_id:params[:iddish])
        open('./app/assets/images/codigo.png', 'wb') do |file|
           file << open(Order.generar_codigo(session[:usuario_activo])).read
        end
    end

    def download_order
        fecha = Time.now.strftime("%Y/%m/%d") 
        data = open(Order.generar_codigo(session[:usuario_activo])).read
        send_data data, :disposition => 'attachment', :filename=>"CodigoPedido"+  fecha +".png"
      end

end
