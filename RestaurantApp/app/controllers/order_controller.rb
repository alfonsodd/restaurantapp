class OrderController < ApplicationController
    skip_before_action :verify_authenticity_token

    def update_order
        Order.update(params[:orderid].to_i, :estado => 1 )
        render json: {msg: "Correcto"}
    end
    
end