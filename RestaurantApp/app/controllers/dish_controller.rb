class DishController < ApplicationController
    skip_before_action :verify_authenticity_token

    def add_dish
        dish = Dish.create(descripcion: params[:descripcion], precio: params[:precio].to_f, menu_id: session[:menu_activo])
        render json: {des: dish.descripcion, pre: dish.precio, id: dish.id}
    end

    def delete_dish
        Dish.delete(params[:dishid])
        render json: {msg:"Plato eliminado"}
    end

end