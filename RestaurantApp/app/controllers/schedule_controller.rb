class ScheduleController < ApplicationController
    skip_before_action :verify_authenticity_token

    def update_schedule
        
        schedule = Schedule.get_schedule_today(session[:usuario_activo].to_i)
        Schedule.update(schedule.id, :apertura => params[:ape].to_i, :cierre => params[:cie].to_i)

        render json: { msg: "Atención actualizada" }
    end
    
end